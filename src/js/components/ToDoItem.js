var React = require('react');

var ToDoItem = React.createClass({
  render: function() {

    var deleteItem = this.props.deleteItem.bind(null, this.props.message.title);

    return (
      <div>
        <div className="col-md-8">
          <div className="row well">
              <span>{this.props.message.title}</span>
          </div>
        </div>
        <div className="col-md-3">
          <div id="crud-buttons">
            <button onClick={deleteItem} className="btn btn-default btn-lg" type="submit">Delete</button>
          </div>
        </div>
      </div>
    );
  }
});

module.exports = ToDoItem;
