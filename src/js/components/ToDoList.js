var React = require('react');
var ToDoItem = require('./ToDoItem');

var ToDoList = React.createClass({
  render: function() {

    var deleteMessage = this.props.deleteItem;

    var list = this.props.messagesList.map(function(messageProp){
      return <ToDoItem key={messageProp.title} deleteItem={deleteMessage} message={messageProp} />
    });

    return <div>
            {list}
           </div>
  }
});

module.exports = ToDoList;
