var React = require('react');
var ToDoList = require('./ToDoList');

var ToDo = React.createClass({
  getInitialState: function() {
    return {
      itemInput : "",
      messages : []
    };
  },
  addItem: function(event) {

    if(event.type == "click" || (event.type == "keypress" && event.key == "Enter")) {
      this.setState({
        itemInput: "",
        messages: this.state.messages.concat(
          {
            title: document.getElementById("to-do-input").value
          }
        )
      });
    }
  },
  handleInput: function(event) {
    this.setState({
      itemInput: event.target.value
    });
  },
  deleteItem: function(item) {
    var items = this.state.messages.filter(function(toDoItem) {
      return toDoItem.title !== item
    });

    this.setState({ messages: items});
  },
  render: function() {
    return (
      <div>
          <div className="col-md-1"></div>
          <div className="col-md-10">
            <div className="form-inline">
              <input type="text" id="to-do-input" className="form-control" placeholder="To Do Input" value={this.state.itemInput} onChange={this.handleInput} onKeyPress={this.addItem}></input>
              <button type="submit" className="btn btn-default" onClick={this.addItem}>Enter</button>
            </div>
            <br />
            <ToDoList deleteItem={this.deleteItem} messagesList={this.state.messages} />
          </div>
     </div>
    );
  }
});

module.exports = ToDo;
