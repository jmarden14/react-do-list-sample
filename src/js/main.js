var React = require('react');
var reactDom = require('react-dom');
var ToDo = require('./components/ToDo');

var App = React.createClass({
  render: function() {
    return (
      <div className="row">
        <ToDo />
      </div>
    );
  }
});

reactDom.render(<App />, document.getElementById('main'));
