var gulp = require('gulp');
var webpack = require('gulp-webpack');
var livereload = require('gulp-livereload');
var named = require('vinyl-named');

gulp.task('webpack', function() {
    gulp.src('./src/js/main.js')
        .pipe(named())
        .pipe(webpack({
          watch: true,
          module: {
            loaders: [
              {
                test: /.jsx?$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: {
                  presets: ['es2015', 'react']
                }
              }
            ],
          },
        }))
        .pipe(gulp.dest('dist/js'));

});

gulp.task('copy', function() {
    gulp.src('src/index.html')
        .pipe(gulp.dest('dist'));

    gulp.src('src/assets/**/*/*.*')
        .pipe(gulp.dest('dist/assets'));
});


gulp.task('default', ['webpack', 'copy'], function () {
    livereload.listen();
    return gulp.watch('src/**/*.*', ['webpack', 'copy']);
});
