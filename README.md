To Run
======

1. Install npm
2. npm install -g gulp
3. npm install -g httpster
4. Run: **npm install**
5. Run: **gulp**
6. cd into the dist folder
7. Use a server such as httpster to run the application
    - cd dist
    - run: httpster
8. Navigate to the localhost port displayed